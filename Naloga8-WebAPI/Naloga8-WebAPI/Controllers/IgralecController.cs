﻿using Microsoft.AspNetCore.Mvc;
using Naloga8_WebAPI.Data;
using Naloga8_WebAPI.Models;
using System.Collections.Generic;
using Newtonsoft.Json;
using System;

namespace Naloga8_WebAPI.Controllers
{
    [ApiController]
    [Route("api/[controller]")]
    public class IgralecController : ControllerBase
    {
        private readonly IIgralecRepository _igralecRepository;

        public IgralecController(IIgralecRepository igralecRepository)
        {
            this._igralecRepository = igralecRepository;
        }

        // pridobimo vse igralce in iskalnik
        [HttpGet]
        public IEnumerable<Igralec> PridobiIgralce([FromQuery]string ime, [FromQuery] string priimek)
        {
            // REST storitev vraca primerke v primeru ce podamo vec parametrov
            // iscemo igralce po imenu in priimku
            if (ime != null && priimek != null)
            {
                List<Igralec> igralci = (List<Igralec>)this._igralecRepository.PridobiIgralce();

                return igralci.FindAll(i => i.Ime == ime && i.Priimek == priimek);
            }

            // REST storitev isce primerke samo po enem parametru
            // poiscemo igralce po imenu
            if (ime != null)
            {
                List<Igralec> igralci = (List<Igralec>)this._igralecRepository.PridobiIgralce();

                return igralci.FindAll(i => i.Ime== ime);
            }

            // poiscemo igralce po priimku
            if (priimek != null)
            {
                List<Igralec> igralci = (List<Igralec>)this._igralecRepository.PridobiIgralce();

                return igralci.FindAll(i => i.Priimek == priimek);
            }

            // ce nismo podali nobenih query parametrov storitev vraca vse primerke
            return this._igralecRepository.PridobiIgralce();
        }

        // prva izmisljena storitev - vrne stevilo vseh igralcev
        [HttpGet]
        [Route("stevilo")]
        public int PridobiSteviloIgralcev()
        {
            IgralecRepository repo = (IgralecRepository)this._igralecRepository;
            
            return repo.PridobiSteviloIgralcev();
        }

        // druga izmisljena storitev - vrne najstarejsega igralca
        [HttpGet]
        [Route("najstarejsi")]
        public Igralec PridobiNajmljajsegaIgralca()
        {
            List<Igralec> seznamIgralcev = (List<Igralec>)this._igralecRepository.PridobiIgralce();

            
            seznamIgralcev.Sort((x, y) => DateTime.Compare(x.DatumRojstva, y.DatumRojstva));

            var najstarejsi = seznamIgralcev[0];

            return najstarejsi;
        }


        // pridobimo specifičnega igralca
        [HttpGet("{id}")]
        public Igralec PridobiIgralca(int id)
        {
            return this._igralecRepository.PridobiIgralca(id);
        }

        // kreiramo novi Klub
        [HttpPost]
        public ActionResult<Klub> DodajKIgralca(Igralec igralec) // 
        {


            var dodaniIgralec = this._igralecRepository.DodajIgralca(igralec);
            this._igralecRepository.Shrani(); // shranimo izbris

            //return dodaniKlub;
            return Ok(dodaniIgralec);
        }

        [HttpPut("{id}")]
        public Igralec IzbrisiIgralca(int id, Igralec igralec)
        {
            var posodobljeniIgralec = this._igralecRepository.PosodobiIgralca(id, igralec);
            this._igralecRepository.Shrani(); // shranimo izbris igralca

            return posodobljeniIgralec;
        }

        // izbrisemo specifičnega igralca
        [HttpDelete("{id}")]
        public Igralec IzbrisiIgralca(int id)
        {
            var izbrisaniIgralec = this._igralecRepository.IzbrisiIgralca(id);
            this._igralecRepository.Shrani(); // shranimo izbris igralca

            return izbrisaniIgralec;
        }
    }
}
