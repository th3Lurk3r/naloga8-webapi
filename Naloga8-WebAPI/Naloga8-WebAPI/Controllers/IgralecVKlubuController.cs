﻿using Microsoft.AspNetCore.Mvc;
using Naloga8_WebAPI.Data;
using Naloga8_WebAPI.Models;
using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Threading.Tasks;

namespace Naloga8_WebAPI.Controllers
{
    [ApiController]
    [Route("api/[controller]")]
    public class IgralecVKlubuController : ControllerBase
    {
        private readonly IIgralecVKlubuRepository _igralecVKlubuRepository;
        private readonly IIgralecRepository _igralecRepository;
        private readonly IKlubRepository _klubRepository;

        public IgralecVKlubuController(IIgralecVKlubuRepository igralecVKlubuRepository, IIgralecRepository igralecRepository, IKlubRepository klubRepository)
        {
            this._igralecVKlubuRepository = igralecVKlubuRepository;
            this._igralecRepository = igralecRepository;
            this._klubRepository = klubRepository;
        }

        // pridobimo vse klube
        [HttpGet]
        public IEnumerable<IgralecVKlubu> PridobiVseIgralecVKlubu()
        {
            return this._igralecVKlubuRepository.PridobiVseIgralecVKlubu();
        }

        // pridobimo specifični klub
        [HttpGet("{id}")]
        public IgralecVKlubu PridobiIgralecVKlubu(int id)
        {
            return this._igralecVKlubuRepository.PridobiIgralecVKlubu(id);
        }

        // kreiramo novi IgralecVKlubu
        [HttpPost]
        public IgralecVKlubu DodajKIgralecVKlubu(IgralecVKlubuDTO igralecVKlubuDTO) 
        {
            var igralec = this._igralecRepository.PridobiIgralca(igralecVKlubuDTO.igralec);
            var klub = this._klubRepository.PridobiKlub(igralecVKlubuDTO.klub);

            Debug.WriteLine("Igralec id: {0}, Klub id: {1}", igralecVKlubuDTO.igralec, igralecVKlubuDTO.klub);

            var noviIgralecVKlubu = new IgralecVKlubu();
            

            //noviIgralecVKlubu.igralec_id = igralecVKlubuDTO.igralec;
            //noviIgralecVKlubu.klub_id = igralecVKlubuDTO.klub;

            Debug.WriteLine("Igralec: {0}, Klub: {1}", igralec, klub);
            
            noviIgralecVKlubu.igralec_ = igralec;
            noviIgralecVKlubu.klub_ = klub;


            noviIgralecVKlubu.LetoOd = igralecVKlubuDTO.LetoOd;
            noviIgralecVKlubu.LetoDo = igralecVKlubuDTO.LetoDo;
            noviIgralecVKlubu.StNastopov = igralecVKlubuDTO.StNastopov;

            noviIgralecVKlubu = this._igralecVKlubuRepository.DodajKIgralecVKlubu(noviIgralecVKlubu);
            this._igralecVKlubuRepository.Shrani(); // shranimo izbris

            return noviIgralecVKlubu;
        }

        //// kreiramo novi IgralecVKlubu
        //[HttpPost]
        //public IgralecVKlubu DodajKIgralecVKlubu(IgralecVKlubu igralecVKlubu) //, [FromBody] IgralecVKlubuDTO igralecKlubDTO
        //{
        //    //var igralec = this._igralecRepository.PridobiIgralca(igralecVKlubu.igralec_.Id);
        //    //var klub = this._klubRepository.PridobiKlub(igralecVKlubu.klub_.Id);

        //    //var igralec = this._igralecRepository.PridobiIgralca(igralec_);
        //    //var klub = this._klubRepository.PridobiKlub(klub_);

        //    int igralec_id = int.Parse(HttpContext.Request.Form["igralec"]);
        //    int klub_id = int.Parse(HttpContext.Request.Form["klub"]);

        //    var igralec = this._igralecRepository.PridobiIgralca(igralec_id);
        //    var klub = this._klubRepository.PridobiKlub(klub_id);

        //    var noviIgralecVKlubu = new IgralecVKlubu();
        //    noviIgralecVKlubu.igralec_ = igralec;
        //    noviIgralecVKlubu.klub_ = klub;

        //    noviIgralecVKlubu = this._igralecVKlubuRepository.DodajKIgralecVKlubu(noviIgralecVKlubu);
        //    this._igralecVKlubuRepository.Shrani(); // shranimo izbris

        //    Console.WriteLine("izbrisaniIIgralecVKlubu: {0}", noviIgralecVKlubu.ToString());
        //    return noviIgralecVKlubu;
        //}

        // posodobimo specifičnega IgralecVKlubu
        [HttpPut("{id}")]
        public IgralecVKlubu PosodobiIgralecVKlubu(int id, IgralecVKlubu igralecVKlubu)
        {
            var posodobljeniIIgralecVKlubu = this._igralecVKlubuRepository.PosodobiIgralecVKlubu(id, igralecVKlubu);
            this._igralecVKlubuRepository.Shrani(); // shranimo 

            return posodobljeniIIgralecVKlubu;
        }

        // izbrisemo specifični IgralecVKlubu
        [HttpDelete("{id}")]
        public IgralecVKlubu IzbrisiIIgralecVKlubu(int id)
        {
            var izbrisaniIIgralecVKlubu = this._igralecVKlubuRepository.IzbrisiIIgralecVKlubu(id);
            this._igralecVKlubuRepository.Shrani(); // shranimo izbris

            return izbrisaniIIgralecVKlubu;
        }
    }
}
