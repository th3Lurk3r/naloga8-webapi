﻿using Microsoft.AspNetCore.Mvc;
using Naloga8_WebAPI.Data;
using Naloga8_WebAPI.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.EntityFrameworkCore;

namespace Naloga8_WebAPI.Controllers
{
    [ApiController]
    [Route("api/[controller]")]
    public class KlubController : ControllerBase
    {
        private readonly IKlubRepository _klubRepository;
        private readonly KosarkaDbContext _context;

        public KlubController(IKlubRepository klubRepository, KosarkaDbContext context)
        {
            this._klubRepository = klubRepository;
            this._context = context;
        }

        // pridobimo vse klube
        public IEnumerable<Klub> PridobiKlube()
        {
            return this._klubRepository.PridobiKlube();
        }

        // pridobimo specifični klub
        [HttpGet("{id}")]
        public Klub PridobiKlub(int id)
        {

            return this._klubRepository.PridobiKlub(id);
        }

        // kreiramo novi Klub
        [HttpPost]
        public ActionResult<Klub> DodajKlub(Klub klub) // 
        {

            var dodaniKlub = this._klubRepository.DodajKlub(klub);
            this._klubRepository.Shrani(); // shranimo izbris

            //Console.WriteLine(klub.IgralecVKlubih.Add);

            Console.WriteLine("izbrisaniIIgralecVKlubu: {0}", dodaniKlub.ToString());
            //return dodaniKlub;

            return Ok(dodaniKlub);
        }

        [HttpPut("{id}")]
        public Klub IzbrisiIKlub(int id, Klub klub)
        {
            var izbrisaniKlub = this._klubRepository.PosodobiKlub(id, klub);
            this._klubRepository.Shrani(); // shranimo izbris

            return izbrisaniKlub;
        }

        // izbrisemo specifični klub
        [HttpDelete("{id}")]
        public Klub IzbrisiIKlub(int id)
        {
            var izbrisaniKlub = this._klubRepository.IzbrisiIKlub(id);
            this._klubRepository.Shrani(); // shranimo izbris

            return izbrisaniKlub;
        }
    }
}
