﻿using Naloga8_WebAPI.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Naloga8_WebAPI.Data
{
    public interface IIgralecRepository
    {
        // pridobi igralca po id-ju
        Igralec PridobiIgralca(int id);
        
        // pridobi vse igralce
        IEnumerable<Igralec> PridobiIgralce();

        // doda igralca 
        Igralec DodajIgralca(Igralec igralec);

        // posodobi igralca po id-ju
        Igralec PosodobiIgralca(int id, Igralec igralec);

        // izbrise igralca po id-ju
        Igralec IzbrisiIgralca(int id);

        int Shrani();
    }
}
