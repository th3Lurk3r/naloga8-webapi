﻿using Naloga8_WebAPI.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Naloga8_WebAPI.Data
{
    public interface IIgralecVKlubuRepository
    {
        // pridobi klub po id-ju
        IgralecVKlubu PridobiIgralecVKlubu(int id);

        // pridobi vse klube
        IEnumerable<IgralecVKlubu> PridobiVseIgralecVKlubu();

        // doda klub 
        IgralecVKlubu DodajKIgralecVKlubu(IgralecVKlubu igralecVKlubu);

        // posodobi klub po id-ju
        IgralecVKlubu PosodobiIgralecVKlubu(int id, IgralecVKlubu igralecVKlubu);

        // izbrise klub po id-ju
        IgralecVKlubu IzbrisiIIgralecVKlubu(int id);

        int Shrani();
    }
}
