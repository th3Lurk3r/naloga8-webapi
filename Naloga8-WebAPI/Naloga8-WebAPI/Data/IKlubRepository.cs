﻿using Naloga8_WebAPI.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Naloga8_WebAPI.Data
{
    public interface IKlubRepository
    {
        // pridobi klub po id-ju
        Klub PridobiKlub(int id);

        // pridobi vse klube
        IEnumerable<Klub> PridobiKlube();

        // doda klub 
        Klub DodajKlub(Klub klub);

        // posodobi klub po id-ju
        Klub PosodobiKlub(int id, Klub klub);

        // izbrise klub po id-ju
        Klub IzbrisiIKlub(int id);

        int Shrani();
    }
}
