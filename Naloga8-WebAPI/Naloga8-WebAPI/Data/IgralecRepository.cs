﻿using Naloga8_WebAPI.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.EntityFrameworkCore;

namespace Naloga8_WebAPI.Data
{
    public class IgralecRepository : IIgralecRepository
    {
        private readonly KosarkaDbContext _context;

        public IgralecRepository(KosarkaDbContext context)
        {
            this._context = context;
        }

        public Igralec DodajIgralca(Igralec igralec)
        {
            var noviIgralec = this._context.Igralecs.Add(igralec);
            //this._context.SaveChanges();

            return noviIgralec.Entity;
        }

        public Igralec IzbrisiIgralca(int id)
        {
            var iskaniIgralec = this.PridobiIgralca(id);
            var izbrisaniIgralec = this._context.Igralecs.Remove(iskaniIgralec);

            return izbrisaniIgralec.Entity;
        }

        public Igralec PosodobiIgralca(int id, Igralec igralec)
        {
            var posodobljeniIgralec = this.PridobiIgralca(id);
            
            posodobljeniIgralec.Ime = igralec.Ime;
            posodobljeniIgralec.Priimek = igralec.Priimek;
            posodobljeniIgralec.DatumRojstva = igralec.DatumRojstva;

            return posodobljeniIgralec;
        }

        public Igralec PridobiIgralca(int id)
        {
            return this._context.Igralecs
                .Include(i=>i.IgralecVKlubih)
                .FirstOrDefault(igralec => igralec.Id == id);
        }

        public IEnumerable<Igralec> PridobiIgralce()
        {
            return this._context.Igralecs
                .Include(i => i.IgralecVKlubih)
                .ToList();
        }

        public int PridobiSteviloIgralcev()
        {
            return this._context.Igralecs.Count();
        }

        public int Shrani()
        {
            return this._context.SaveChanges();
        }
    }
}
