﻿using Naloga8_WebAPI.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.EntityFrameworkCore;

namespace Naloga8_WebAPI.Data
{
    public class IgralecVKlubuRepository : IIgralecVKlubuRepository
    {
        private readonly KosarkaDbContext _context;

        public IgralecVKlubuRepository()
        {
        }

        public IgralecVKlubuRepository(KosarkaDbContext context)
        {
            this._context = context;
        }

        public IgralecVKlubu DodajKIgralecVKlubu(IgralecVKlubu igralecVKlubu)
        {
            var noviIgralecVKlubu = this._context.IgralecVKlubus.Add(igralecVKlubu);

            return noviIgralecVKlubu.Entity;
        }

        public IgralecVKlubu IzbrisiIIgralecVKlubu(int id)
        {
            var iskaniIgralecVKlubu = this.PridobiIgralecVKlubu(id);
            var izbrisaniIgralecVKlubu = this._context.IgralecVKlubus.Remove(iskaniIgralecVKlubu);

            return izbrisaniIgralecVKlubu.Entity;
        }

        public IgralecVKlubu PosodobiIgralecVKlubu(int id, IgralecVKlubu igralecVKlubu)
        {
            var posodobljeniIgralecVKlubu = this.PridobiIgralecVKlubu(id);

            posodobljeniIgralecVKlubu.igralec_ = igralecVKlubu.igralec_;
            posodobljeniIgralecVKlubu.klub_ = igralecVKlubu.klub_;
            posodobljeniIgralecVKlubu.LetoDo = igralecVKlubu.LetoDo;
            posodobljeniIgralecVKlubu.LetoOd = igralecVKlubu.LetoOd;
            posodobljeniIgralecVKlubu.StNastopov = igralecVKlubu.StNastopov;

            return posodobljeniIgralecVKlubu;
        }

        public IgralecVKlubu PridobiIgralecVKlubu(int id)
        {
            return this._context.IgralecVKlubus
                .Include(ivk => ivk.igralec_)
                .Include(ivk => ivk.klub_)
                .FirstOrDefault(igralecVKlubu => igralecVKlubu.Id == id);


            //return this._context.IgralecVKlubus.FirstOrDefault(igralecVKlubu => igralecVKlubu.Id == id);
        }

        public IEnumerable<IgralecVKlubu> PridobiVseIgralecVKlubu()
        {
            return this._context.IgralecVKlubus
                .Include(ivk => ivk.igralec_)
                .Include(ivk => ivk.klub_)
                .ToList();
        }

        public int Shrani()
        {
            return this._context.SaveChanges();
        }
    }
}
