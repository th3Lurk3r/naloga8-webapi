﻿using Naloga8_WebAPI.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.EntityFrameworkCore;

namespace Naloga8_WebAPI.Data
{
    public class KlubRepository : IKlubRepository
    {
        private readonly KosarkaDbContext _context;

        public KlubRepository(KosarkaDbContext context)
        {
            this._context = context;
        }

        public Klub DodajKlub(Klub klub)
        {
            var noviKlub = this._context.Klubs.Add(klub);
            //this._context.SaveChanges();

            return noviKlub.Entity;
        }

        public Klub IzbrisiIKlub(int id)
        {
            var iskaniKlub = this.PridobiKlub(id);
            var izbrisaniKlub = this._context.Klubs.Remove(iskaniKlub);

            return izbrisaniKlub.Entity;
        }

        public Klub PosodobiKlub(int id, Klub klub)
        {
            var posodobljeniKlub = this.PridobiKlub(id);

            posodobljeniKlub.Naziv = klub.Naziv;
            posodobljeniKlub.Telovadnica = klub.Telovadnica;
            posodobljeniKlub.LetoUstanovitve = klub.LetoUstanovitve;

            return posodobljeniKlub;
        }

        public Klub PridobiKlub(int id)
        {
            return this._context.Klubs
                .Include(k => k.IgralecVKlubih)
                .FirstOrDefault(klub => klub.Id == id);

            //return this._context.Klubs.FirstOrDefault(klub => klub.Id == id);
        }

        public IEnumerable<Klub> PridobiKlube()
        {
            return this._context.Klubs
                //.Include(k => k.IgralecVKlubih)
                .ToList();
        }

        public int Shrani()
        {
            return this._context.SaveChanges();
        }
    }
}
