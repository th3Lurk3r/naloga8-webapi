﻿using Naloga8_WebAPI.Models;
using System;
using System.Collections.Generic;
//using System.Data.Entity;
using System.Linq;
using System.Web;
using Microsoft.EntityFrameworkCore;
using System.Diagnostics.CodeAnalysis;

namespace Naloga8_WebAPI.Data
{
    public class KosarkaDbContext : DbContext
    {
        public KosarkaDbContext([NotNullAttribute] DbContextOptions options) : base(options) { }
        public DbSet<IgralecVKlubu> IgralecVKlubus { get; set; }
        public DbSet<Igralec> Igralecs { get; set; }
        public DbSet<Klub> Klubs { get; set; }
    }
}