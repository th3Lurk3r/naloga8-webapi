﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.Web;

namespace Naloga8_WebAPI.Models
{
    public class Igralec
    {
        public int Id { get; set; }
        public string Ime { get; set; }
        public string Priimek { get; set; }
        public DateTime DatumRojstva { get; set; }
        public virtual ICollection<IgralecVKlubu> IgralecVKlubih { get; set; }

        public Igralec()
        {
        }

        public Igralec(string ime, string priimek, DateTime datumRojstva)
        {
            Ime = ime;
            Priimek = priimek;
            DatumRojstva = datumRojstva;
        }
    }
}