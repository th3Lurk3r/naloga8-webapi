﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Runtime.Serialization;
using System.Web;

namespace Naloga8_WebAPI.Models
{
    public class IgralecVKlubu
    {
        public int Id { get; set; }
        public virtual Igralec igralec_ { get; set; }
        public virtual Klub klub_ { get; set; }
        public int igralec_id { get; set; }
        public int klub_id { get; set; }
        public int LetoOd { get; set; }
        public int LetoDo { get; set; }
        public int StNastopov { get; set; }
        public IgralecVKlubu(Igralec igralec, Klub klub, int letoOd, int letoDo, int stNastopov)
        {
            this.igralec_ = igralec;
            this.klub_ = klub;
            LetoOd = letoOd;
            LetoDo = letoDo;
            StNastopov = stNastopov;
        }

        public IgralecVKlubu()
        {
        }
    }
}