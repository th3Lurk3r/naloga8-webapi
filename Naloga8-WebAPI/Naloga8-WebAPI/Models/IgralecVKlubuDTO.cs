﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Naloga8_WebAPI.Models
{
    public class IgralecVKlubuDTO
    {
        public IgralecVKlubuDTO()
        {
        }

        public IgralecVKlubuDTO(int igralec, int klub, int letoOd, int letoDo, int stNastopov)
        {
            LetoOd = letoOd;
            LetoDo = letoDo;
            StNastopov = stNastopov;
        }

        public int igralec { get; set; }
        public int klub { get; set; }
        public int LetoOd { get; set; }
        public int LetoDo { get; set; }
        public int StNastopov { get; set; }

    }
}
