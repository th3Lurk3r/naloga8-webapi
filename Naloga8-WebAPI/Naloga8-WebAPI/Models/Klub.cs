﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.Web;

namespace Naloga8_WebAPI.Models
{
    public class Klub
    {
        public int Id { get; set; }
        public string Naziv { get; set; }
        public string Telovadnica { get; set; }
        public int LetoUstanovitve { get; set; }
        public virtual ICollection<IgralecVKlubu> IgralecVKlubih { get; set; }

        public Klub()
        {
        }

        public Klub(string naziv, string telovadnica, int letoUstanovitve)
        {
            Naziv = naziv;
            Telovadnica = telovadnica;
            LetoUstanovitve = letoUstanovitve;
        }
    }
}