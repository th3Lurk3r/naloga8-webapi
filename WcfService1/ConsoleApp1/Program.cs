﻿using ConsoleApp1.ServiceReference1;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ConsoleApp1
{
    class Program
    {
        static ServiceReference1.Service1Client servis;
        static void Main(string[] args)
        {
            int privilegij = 0;

            servis = new ServiceReference1.Service1Client();


            do
            {
                string ime, geslo;
                Console.WriteLine("Vnesite up. ime");
                ime = Console.ReadLine();
                Console.WriteLine("Vnesite up. geslo");
                geslo = Console.ReadLine();

                int status = servis.vpis(ime, geslo);
                privilegij = status;

                if (privilegij == 0)
                {
                    Console.WriteLine("Napačni podatki poizkusite ponovno");
                }
            } while (privilegij < 1);

            int izbranServis;

            Console.WriteLine("Uspešna prijava");

            do
            {
                Console.WriteLine("Izberite metodo");
                Console.WriteLine("1.- vrniVseIgralce()");
                Console.WriteLine("2.- vrniVseKlube();");
                Console.WriteLine("3.- vrniIgralca(int id)");
                Console.WriteLine("4.- vrniKlub(int id)");
                Console.WriteLine("5.- klubZNajvecIgralci()");
                Console.WriteLine("6.- najstarejsiIgralec()");
                Console.WriteLine("7.- povprecnaStarostIgralcevVKlubu()");
                Console.WriteLine("8.- dodajIgralca(string ime, string priimek, DateTime datumRojstva)");
                Console.WriteLine("9.- dodajKlub(string naziv, string telovadnica, int letoUstanovitve)");
                Console.WriteLine("10.- dodajIgralcuKlub(int igralecId, int klubId, int letoOd, int letoDo, int stNastopov)");
                Console.WriteLine("11.- odstraniIgralcaIzKluba(int igralecId, int klubId)");
                Console.WriteLine("12.- izbrisiIgralca(int id)");
                Console.WriteLine("13.- izbrisiKlub(int id)");
                Console.WriteLine("14.- spremeniIgralca(int id, string ime, string priimek, DateTime datumRojstva)");
                Console.WriteLine("15.- spremeniKlub(int id, string naziv, string telovadnica, int letoUstanovitve)");

                izbranServis = int.Parse(Console.ReadLine());

                switch (izbranServis)
                {
                    case 1:
                        if (privilegij == 2)
                        {
                           vrniVseIgralce();
                        }
                        else
                        {
                            Console.WriteLine("Nimate potrebnega dovoljenja za ogled vsebine");
                        }
                        break;
                    case 2:
                        if (privilegij == 2)
                        {
                            vrniVseKlube();
                        }
                        else
                        {
                            Console.WriteLine("Nimate potrebnega dovoljenja za ogled vsebine");
                        }
                        break;
                    case 3:
                        vrniIgralca();
                        break;
                    case 4:
                        vrniKlub();
                        break;
                    case 5:
                        klubZNajvecIgralci();
                        break;
                    case 6:
                        najstarejsiIgralec();
                        break;
                    case 7:
                        povprecnaStarostIgralcevVKlubu();
                        break;
                   
                    case 8:
                        dodajIgralca();
                        break;
                    case 9:
                        dodajKlub();
                        break;
                    case 10:
                        dodajIgralcuKlub();
                        break;
                    case 11:
                        odstraniIgralcaIzKluba();
                        break;
                    case 12:
                        izbrisiIgralca();
                        break;
                    case 13:
                        izbrisiKlub();
                        break;
                    case 14:
                        spremeniIgralca();
                        break;
                    case 15:
                        spremeniKlub();
                        break;
                    default:
                        Console.WriteLine("Izbrana metoda ne obstaja");
                        break;
                }
            } while (true);
            Console.ReadLine();
        }

        public static void klubZNajvecIgralci()
        {
            var klub = servis.klubZNajvecIgralci();
            Console.WriteLine("Klub, ki ima največ igralcev: " + klub.Naziv);

        }

        public static void najstarejsiIgralec()
        {
            var igralec = servis.najstarejsiIgralec();
            Console.WriteLine("Najstarejši igralec: " + igralec.Ime + " " + igralec.Priimek);
        }

        public static void povprecnaStarostIgralcevVKlubu()
        {
            Console.WriteLine("Vpisite ID klub");
            int id = int.Parse(Console.ReadLine());
            Console.WriteLine("Povprecna starost igralce v klubu: " + servis.povprecnaStarostIgralcevVKlubu(id));
        }

        public static void vrniIgralca()
        {
            Console.WriteLine("Vpisite ID igralca");
            int id = int.Parse(Console.ReadLine());
            var igralec = servis.vrniIgralca(id);
            Console.WriteLine(igralec.Ime + " " + igralec.Priimek + " " + igralec.DatumRojstva.ToString());
        }

        public static void vrniKlub()
        {
            Console.WriteLine("Vpisite ID kluba");
            int id = int.Parse(Console.ReadLine());
            var klub = servis.vrniKlub(id);
            Console.WriteLine(klub.Naziv + " " + klub.Telovadnica + " " + klub.LetoUstanovitve.ToString());
        }

        public static void vrniVseIgralce()
        {
            Console.WriteLine("Vsi igralci:");

            foreach (var igralec in servis.vrniVseIgralce())
            {
                Console.WriteLine(igralec.Id + " " + igralec.Ime + " " + igralec.Priimek);
            }
        }

        public static void vrniVseKlube()
        {
            Console.WriteLine("Vsi klubi:");

            foreach (var klub in servis.vrniVseKlube())
            {
                Console.WriteLine(klub.Naziv + " " + klub.Telovadnica + klub.LetoUstanovitve.ToString());
            }
        }

        public static void dodajKlub()
        {
            Console.WriteLine("Vnesi naziv:");
            string naziv = Console.ReadLine();
            Console.WriteLine("Vnesi telovadnico:");
            string telovadnica = Console.ReadLine();
            Console.WriteLine("Vnesi letoUstanovitve:");
            int letoUstanovitve = int.Parse(Console.ReadLine());

            servis.dodajKlub(naziv, telovadnica, letoUstanovitve);
        }
        public static void dodajIgralca()
        {
            Console.WriteLine("Vnesi ime:");
            string ime = Console.ReadLine();
            Console.WriteLine("Vnesi priimek:");
            string priimek = Console.ReadLine();
            Console.WriteLine("Vnesi datum:");
            string datumStringa = Console.ReadLine();
            string[] datuma = datumStringa.Split('-');

            servis.dodajIgralca(ime, priimek, new DateTime(int.Parse(datuma[0]), int.Parse(datuma[1]), int.Parse(datuma[2])));
 
        }
        public static void izbrisiKlub()
        {
            Console.WriteLine("Vnesi ID kluba:");
            int id = int.Parse(Console.ReadLine());
            servis.izbrisiKlub(id);
        }
        public static void izbrisiIgralca()
        {
            Console.WriteLine("Vnesi ID igralca:");
            int id = int.Parse(Console.ReadLine());
            servis.izbrisiIgralca(id);
        }
        public static void spremeniKlub()
        {
            Console.WriteLine("Vnesi ID kluba:");
            int id = int.Parse(Console.ReadLine());
            Console.WriteLine("Vnesi naziv:");
            string naziv = Console.ReadLine();
            Console.WriteLine("Vnesi telovadnico:");
            string telovadnica = Console.ReadLine();
            Console.WriteLine("Vnesi letoUstanovitve:");
            int letoUstanovitve = int.Parse(Console.ReadLine());

            servis.spremeniKlub(id, naziv, telovadnica, letoUstanovitve);
        }

        public static void spremeniIgralca()
        {
            Console.WriteLine("Vnesi ID igralca:");
            int id = int.Parse(Console.ReadLine());
            Console.WriteLine("Vnesi ime:");
            string ime = Console.ReadLine();
            Console.WriteLine("Vnesi priimek:");
            string priimek = Console.ReadLine();
            Console.WriteLine("Vnesi datum:");
            string datumStringa = Console.ReadLine();
            string[] datuma = datumStringa.Split('-');

            servis.spremeniIgralca(id, ime, priimek, new DateTime(int.Parse(datuma[0]), int.Parse(datuma[1]), int.Parse(datuma[2])));
        }
        public static void dodajIgralcuKlub()
        {
            Console.WriteLine("Vnesi ID igralca:");
            int igralecId = int.Parse(Console.ReadLine());
            Console.WriteLine("Vnesi ID kluba:");
            int klubId = int.Parse(Console.ReadLine());
            Console.WriteLine("Vnesi leto od prestopa:");
            int letoOd = int.Parse(Console.ReadLine());
            Console.WriteLine("Vnesi leto do konca prestopa:");
            int letoDo = int.Parse(Console.ReadLine());

            servis.dodajIgralcuKlub(igralecId,klubId,letoOd,letoDo,0);
        }
        public static void odstraniIgralcaIzKluba()
        {
            Console.WriteLine("Vnesi ID igralca:");
            int igralecId = int.Parse(Console.ReadLine());
            Console.WriteLine("Vnesi ID kluba:");
            int klubId = int.Parse(Console.ReadLine());

            servis.odstraniIgralcaIzKluba(igralecId, klubId);
        }
    }
}
