﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.ServiceModel;
using System.ServiceModel.Web;
using System.Text;

namespace WcfService1
{
    // NOTE: You can use the "Rename" command on the "Refactor" menu to change the interface name "IService1" in both code and config file together.
    [ServiceContract]
    public interface IService1
    {
        [OperationContract]
        int vpis(string ime, string geslo);
        [OperationContract]
        List<Igralec> vrniVseIgralce();
        [OperationContract]
        List<Klub> vrniVseKlube();
        [OperationContract]
        Igralec vrniIgralca(int id);
        [OperationContract]
        Klub vrniKlub(int id);
        [OperationContract]
        Klub klubZNajvecIgralci();
        [OperationContract]
        Igralec najstarejsiIgralec();
        [OperationContract]
        double povprecnaStarostIgralcevVKlubu(int id);
        [OperationContract]
        void dodajIgralca(string ime, string priimek, DateTime datumRojstva);
        [OperationContract]
        void dodajKlub(string naziv, string telovadnica, int letoUstanovitve);
        [OperationContract]
        void dodajIgralcuKlub(int igralecId, int klubId, int letoOd, int letoDo, int stNastopov);
        [OperationContract]
        void izbrisiIgralca(int id);
        [OperationContract]
        void izbrisiKlub(int id);
        [OperationContract]
        void odstraniIgralcaIzKluba(int igralecId, int klubId);
        [OperationContract]
        void spremeniKlub(int id, string naziv, string telovadnica, int letoUstanovitve);
        [OperationContract]
        void spremeniIgralca(int id, string ime, string priimek, DateTime datumRojstva);
        [OperationContract]
        List<IgralecVKlubu> vrniVseIgralceVKlubih();
    }
}
