﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.Web;

namespace WcfService1
{
    [DataContract]
    public class Igralec
    {
        [DataMember]
        public int Id { get; set; }
        [DataMember]
        public string Ime { get; set; }
        [DataMember]
        public string Priimek { get; set; }
        [DataMember]
        public DateTime DatumRojstva { get; set; }
        [DataMember]
        public virtual ICollection<IgralecVKlubu> IgralecVKlubih { get; set; }


        public Igralec()
        {
        }

        public Igralec(string ime, string priimek, DateTime datumRojstva)
        {
            Ime = ime;
            Priimek = priimek;
            DatumRojstva = datumRojstva;
        }
    }
}