﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Runtime.Serialization;
using System.Web;

namespace WcfService1
{
    [DataContract]
    public class IgralecVKlubu
    {
        [DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        [DataMember]
        [Key]
        public int Id { get; set; }
        [DataMember]
        public Igralec igralec { get; set; }
        [DataMember]
        public Klub klub { get; set; }
        [DataMember]
        public int LetoOd { get; set; }
        [DataMember]
        public int LetoDo { get; set; }
        [DataMember]
        public int StNastopov { get; set; }

        public IgralecVKlubu()
        {
        }

        public IgralecVKlubu(Igralec igralec, Klub klub, int letoOd, int letoDo, int stNastopov)
        {
            this.igralec = igralec;
            this.klub = klub;
            LetoOd = letoOd;
            LetoDo = letoDo;
            StNastopov = stNastopov;
        }
    }
}