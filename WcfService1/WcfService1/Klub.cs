﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.Web;

namespace WcfService1
{
    [DataContract]
    public class Klub
    {
        [DataMember]
        public int Id { get; set; }
        [DataMember]
        public string Naziv { get; set; }
        [DataMember]
        public string Telovadnica { get; set; }
        [DataMember]
        public int LetoUstanovitve { get; set; }
        [DataMember]
        public virtual ICollection<IgralecVKlubu> IgralecVKlubih { get; set; }

        public Klub()
        {
        }

        public Klub(string naziv, string telovadnica, int letoUstanovitve)
        {
            Naziv = naziv;
            Telovadnica = telovadnica;
            LetoUstanovitve = letoUstanovitve;
        }
    }
}