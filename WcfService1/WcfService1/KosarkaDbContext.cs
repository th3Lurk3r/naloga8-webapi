﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Web;

namespace WcfService1
{
    public class KosarkaDbContext : DbContext
    {
        public KosarkaDbContext() { }
        public DbSet<IgralecVKlubu> IgralciVKlubih { get; set; }
        public DbSet<Igralec> Igralci { get; set; }
        public DbSet<Klub> Klubi { get; set; }
    }
}