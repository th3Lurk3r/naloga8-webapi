﻿namespace WcfService1.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class initialmigration : DbMigration
    {
        public override void Up()
        {
            CreateTable(
                "dbo.Igralecs",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        Ime = c.String(),
                        Priimek = c.String(),
                        DatumRojstva = c.DateTime(nullable: false),
                    })
                .PrimaryKey(t => t.Id);
            
            CreateTable(
                "dbo.IgralecVKlubus",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        LetoOd = c.Int(nullable: false),
                        LetoDo = c.Int(nullable: false),
                        StNastopov = c.Int(nullable: false),
                        igralec_Id = c.Int(),
                        klub_Id = c.Int(),
                    })
                .PrimaryKey(t => t.Id)
                .ForeignKey("dbo.Igralecs", t => t.igralec_Id)
                .ForeignKey("dbo.Klubs", t => t.klub_Id)
                .Index(t => t.igralec_Id)
                .Index(t => t.klub_Id);
            
            CreateTable(
                "dbo.Klubs",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        Naziv = c.String(),
                        Telovadnica = c.String(),
                        LetoUstanovitve = c.Int(nullable: false),
                    })
                .PrimaryKey(t => t.Id);
            
        }
        
        public override void Down()
        {
            DropForeignKey("dbo.IgralecVKlubus", "klub_Id", "dbo.Klubs");
            DropForeignKey("dbo.IgralecVKlubus", "igralec_Id", "dbo.Igralecs");
            DropIndex("dbo.IgralecVKlubus", new[] { "klub_Id" });
            DropIndex("dbo.IgralecVKlubus", new[] { "igralec_Id" });
            DropTable("dbo.Klubs");
            DropTable("dbo.IgralecVKlubus");
            DropTable("dbo.Igralecs");
        }
    }
}
