﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.ServiceModel;
using System.ServiceModel.Web;
using System.Text;

namespace WcfService1
{
    // NOTE: You can use the "Rename" command on the "Refactor" menu to change the class name "Service1" in code, svc and config file together.
    // NOTE: In order to launch WCF Test Client for testing this service, please select Service1.svc or Service1.svc.cs at the Solution Explorer and start debugging.
    [ServiceBehavior(InstanceContextMode = InstanceContextMode.Single)]
    public class Service1 : IService1
    {
        List<UporabniskiRacun> uporabniskiRacuni;

        public Service1()
        {
            uporabniskiRacuni = new List<UporabniskiRacun>();

            uporabniskiRacuni.Add(new UporabniskiRacun("admin", "admin", true));
            uporabniskiRacuni.Add(new UporabniskiRacun("gost", "gost", false));
        }
        public Klub klubZNajvecIgralci()
        {
            using (KosarkaDbContext kosarkaDbContext = new KosarkaDbContext())
            {
                kosarkaDbContext.Configuration.ProxyCreationEnabled = false;
                return kosarkaDbContext.IgralciVKlubih.GroupBy(x => x.klub)
                                 .OrderByDescending(gp => gp.Count())
                                 .Select(g => g.Key)
                                 .ToList()
                                 .First();
            }
        }

        public void dodajIgralca(string ime, string priimek, DateTime datumRojstva)
        {

            using (KosarkaDbContext kosarkaDbContext = new KosarkaDbContext())
            {
                Igralec igralec = new Igralec(ime, priimek, datumRojstva);
                kosarkaDbContext.Igralci.Add(igralec);
                kosarkaDbContext.SaveChanges();
            }
        }

        public void dodajKlub(string naziv, string telovadnica, int letoUstanovitve)
        {
            using (KosarkaDbContext kosarkaDbContext = new KosarkaDbContext())
            {
                Klub klub = new Klub(naziv, telovadnica, letoUstanovitve);
                kosarkaDbContext.Klubi.Add(klub);
                kosarkaDbContext.SaveChanges();
            }
        }


        public void dodajIgralcuKlub(int igralecId, int klubId, int letoOd, int letoDo, int stNastopov)
        {
            using (KosarkaDbContext kosarkaDbContext = new KosarkaDbContext())
            {
                kosarkaDbContext.Configuration.ProxyCreationEnabled = false;
                Igralec igralec = kosarkaDbContext.Igralci.Where(x => x.Id == igralecId).First();
                Klub klub = kosarkaDbContext.Klubi.Where(x => x.Id == klubId).First();

                IgralecVKlubu igralecVKlubu = new IgralecVKlubu(igralec, klub, letoOd, letoDo, stNastopov);

                kosarkaDbContext.IgralciVKlubih.Add(igralecVKlubu);
                kosarkaDbContext.SaveChanges();
            }
        }

        public void odstraniIgralcaIzKluba(int igralecId, int klubId)
        {
            using (KosarkaDbContext kosarkaDbContext = new KosarkaDbContext())
            {
                kosarkaDbContext.Configuration.ProxyCreationEnabled = false;
                IgralecVKlubu igralecVKlubu = kosarkaDbContext.IgralciVKlubih.Where(x=>x.igralec.Id == igralecId && x.klub.Id == klubId).First();
                kosarkaDbContext.IgralciVKlubih.Remove(igralecVKlubu);
                kosarkaDbContext.SaveChanges();
            }
        }

        public void spremeniKlub(int id, string naziv, string telovadnica, int letoUstanovitve)
        {
            using (KosarkaDbContext kosarkaDbContext = new KosarkaDbContext())
            {
                kosarkaDbContext.Configuration.ProxyCreationEnabled = false;
                Klub klub = kosarkaDbContext.Klubi.Where(x => x.Id == id).First();
                klub.Naziv = naziv;
                klub.Telovadnica = telovadnica;
                klub.LetoUstanovitve = letoUstanovitve;
                kosarkaDbContext.SaveChanges();
            }
        }

        public void spremeniIgralca(int id, string ime, string priimek, DateTime datumRojstva)
        {
            using (KosarkaDbContext kosarkaDbContext = new KosarkaDbContext())
            {
                kosarkaDbContext.Configuration.ProxyCreationEnabled = false;
                Igralec igralec = kosarkaDbContext.Igralci.Where(x => x.Id == id).First();
                igralec.Ime = ime;
                igralec.Priimek = priimek;
                igralec.DatumRojstva = datumRojstva;
                kosarkaDbContext.SaveChanges();
            }
        }

        public void izbrisiIgralca(int id)
        {
            using (KosarkaDbContext kosarkaDbContext = new KosarkaDbContext())
            {
                kosarkaDbContext.Configuration.ProxyCreationEnabled = false;
                Igralec igralec = kosarkaDbContext.Igralci.Where(x => x.Id == id).First();
                kosarkaDbContext.Igralci.Remove(igralec);
                kosarkaDbContext.SaveChanges();
            }
        }

        public void izbrisiKlub(int id)
        {
            using (KosarkaDbContext kosarkaDbContext = new KosarkaDbContext())
            {
                kosarkaDbContext.Configuration.ProxyCreationEnabled = false;
                Klub klub = kosarkaDbContext.Klubi.Where(x => x.Id == id).First();
                kosarkaDbContext.Klubi.Remove(klub);
                kosarkaDbContext.SaveChanges();
            }
        }

        public Igralec najstarejsiIgralec()
        {
            using (KosarkaDbContext kosarkaDbContext = new KosarkaDbContext())
            {
                kosarkaDbContext.Configuration.ProxyCreationEnabled = false;
                return kosarkaDbContext.Igralci.OrderBy(x => x.DatumRojstva).First();
            }
        }

        public double povprecnaStarostIgralcevVKlubu(int id)
        {
            using (KosarkaDbContext kosarkaDbContext = new KosarkaDbContext())
            {
                kosarkaDbContext.Configuration.ProxyCreationEnabled = false;
                return (kosarkaDbContext.Igralci.Average(x => (DateTime.Now - x.DatumRojstva).TotalDays)) / 365;
            }
        }

        public int vpis(string ime, string geslo)
        {
            int status = 0;
            UporabniskiRacun ur;
            ur = (from racun in uporabniskiRacuni
                  where racun.Ime == ime && racun.Geslo == geslo
                  select racun).First();


            if (ur != null)
            {
                status = 1;
                if (ur.Admin == true) status = 2;
            }

            return status;
        }

        public Igralec vrniIgralca(int id)
        {
            using (KosarkaDbContext kosarkaDbContext = new KosarkaDbContext())
            {
                kosarkaDbContext.Configuration.ProxyCreationEnabled = false;
                return kosarkaDbContext.Igralci.Where(x => x.Id == id).First();
            }
        }

        public Klub vrniKlub(int id)
        {
            using (KosarkaDbContext kosarkaDbContext = new KosarkaDbContext())
            {
                kosarkaDbContext.Configuration.ProxyCreationEnabled = false;
                return kosarkaDbContext.Klubi.Where(x => x.Id == id).First();
            }
        }

        public List<Igralec> vrniVseIgralce()
        {
            using (KosarkaDbContext kosarkaDbContext = new KosarkaDbContext())
            {
                kosarkaDbContext.Configuration.ProxyCreationEnabled = false;
                return kosarkaDbContext.Igralci.ToList();
            }
        }

        public List<IgralecVKlubu> vrniVseIgralceVKlubih()
        {
            using (KosarkaDbContext kosarkaDbContext = new KosarkaDbContext())
            {
                kosarkaDbContext.Configuration.ProxyCreationEnabled = false;
                return kosarkaDbContext.IgralciVKlubih.ToList();
            }
        }

        public List<Klub> vrniVseKlube()
        {
            using (KosarkaDbContext kosarkaDbContext = new KosarkaDbContext())
            {
                kosarkaDbContext.Configuration.ProxyCreationEnabled = false;
                return kosarkaDbContext.Klubi.ToList();
            }
        }
    }
}
