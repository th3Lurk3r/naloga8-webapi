﻿//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated by a tool.
//
//     Changes to this file may cause incorrect behavior and will be lost if
//     the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

namespace ServiceReference1
{
    using System;
    using System.Collections;
    using System.Runtime.Serialization;
    using System.Threading.Tasks;

    [System.Diagnostics.DebuggerStepThroughAttribute()]
    [System.CodeDom.Compiler.GeneratedCodeAttribute("Microsoft.Tools.ServiceModel.Svcutil", "2.0.2")]
    [System.Runtime.Serialization.DataContractAttribute(Name="Igralec", Namespace="http://schemas.datacontract.org/2004/07/WcfService1")]
    public partial class Igralec : object
    {
        
        private System.DateTime DatumRojstvaField;
        
        private int IdField;
        
        private ServiceReference1.IgralecVKlubu[] IgralecVKlubihField;
        
        private string ImeField;
        
        private string PriimekField;
        
        [System.Runtime.Serialization.DataMemberAttribute()]
        public System.DateTime DatumRojstva
        {
            get
            {
                return this.DatumRojstvaField;
            }
            set
            {
                this.DatumRojstvaField = value;
            }
        }
        
        [System.Runtime.Serialization.DataMemberAttribute()]
        public int Id
        {
            get
            {
                return this.IdField;
            }
            set
            {
                this.IdField = value;
            }
        }
        
        [System.Runtime.Serialization.DataMemberAttribute()]
        public ServiceReference1.IgralecVKlubu[] IgralecVKlubih
        {
            get
            {
                return this.IgralecVKlubihField;
            }
            set
            {
                this.IgralecVKlubihField = value;
            }
        }
        
        [System.Runtime.Serialization.DataMemberAttribute()]
        public string Ime
        {
            get
            {
                return this.ImeField;
            }
            set
            {
                this.ImeField = value;
            }
        }
        
        [System.Runtime.Serialization.DataMemberAttribute()]
        public string Priimek
        {
            get
            {
                return this.PriimekField;
            }
            set
            {
                this.PriimekField = value;
            }
        }
    }
    
    [System.Diagnostics.DebuggerStepThroughAttribute()]
    [System.CodeDom.Compiler.GeneratedCodeAttribute("Microsoft.Tools.ServiceModel.Svcutil", "2.0.2")]
    [System.Runtime.Serialization.DataContractAttribute(Name="IgralecVKlubu", Namespace="http://schemas.datacontract.org/2004/07/WcfService1")]
    public partial class IgralecVKlubu : object
    {
        
        private int IdField;
        
        private int LetoDoField;
        
        private int LetoOdField;
        
        private int StNastopovField;
        
        private ServiceReference1.Igralec igralecField;
        
        private ServiceReference1.Klub klubField;
        
        [System.Runtime.Serialization.DataMemberAttribute()]
        public int Id
        {
            get
            {
                return this.IdField;
            }
            set
            {
                this.IdField = value;
            }
        }
        
        [System.Runtime.Serialization.DataMemberAttribute()]
        public int LetoDo
        {
            get
            {
                return this.LetoDoField;
            }
            set
            {
                this.LetoDoField = value;
            }
        }
        
        [System.Runtime.Serialization.DataMemberAttribute()]
        public int LetoOd
        {
            get
            {
                return this.LetoOdField;
            }
            set
            {
                this.LetoOdField = value;
            }
        }
        
        [System.Runtime.Serialization.DataMemberAttribute()]
        public int StNastopov
        {
            get
            {
                return this.StNastopovField;
            }
            set
            {
                this.StNastopovField = value;
            }
        }
        
        [System.Runtime.Serialization.DataMemberAttribute()]
        public ServiceReference1.Igralec igralec
        {
            get
            {
                return this.igralecField;
            }
            set
            {
                this.igralecField = value;
            }
        }
        
        [System.Runtime.Serialization.DataMemberAttribute()]
        public ServiceReference1.Klub klub
        {
            get
            {
                return this.klubField;
            }
            set
            {
                this.klubField = value;
            }
        }
    }
    
    [System.Diagnostics.DebuggerStepThroughAttribute()]
    [System.CodeDom.Compiler.GeneratedCodeAttribute("Microsoft.Tools.ServiceModel.Svcutil", "2.0.2")]
    [System.Runtime.Serialization.DataContractAttribute(Name="Klub", Namespace="http://schemas.datacontract.org/2004/07/WcfService1")]
    public partial class Klub : object
    {
        
        private int IdField;
        
        private ServiceReference1.IgralecVKlubu[] IgralecVKlubihField;
        
        private int LetoUstanovitveField;
        
        private string NazivField;
        
        private string TelovadnicaField;
        
        [System.Runtime.Serialization.DataMemberAttribute()]
        public int Id
        {
            get
            {
                return this.IdField;
            }
            set
            {
                this.IdField = value;
            }
        }
        
        [System.Runtime.Serialization.DataMemberAttribute()]
        public ServiceReference1.IgralecVKlubu[] IgralecVKlubih
        {
            get
            {
                return this.IgralecVKlubihField;
            }
            set
            {
                this.IgralecVKlubihField = value;
            }
        }
        
        [System.Runtime.Serialization.DataMemberAttribute()]
        public int LetoUstanovitve
        {
            get
            {
                return this.LetoUstanovitveField;
            }
            set
            {
                this.LetoUstanovitveField = value;
            }
        }
        
        [System.Runtime.Serialization.DataMemberAttribute()]
        public string Naziv
        {
            get
            {
                return this.NazivField;
            }
            set
            {
                this.NazivField = value;
            }
        }
        
        [System.Runtime.Serialization.DataMemberAttribute()]
        public string Telovadnica
        {
            get
            {
                return this.TelovadnicaField;
            }
            set
            {
                this.TelovadnicaField = value;
            }
        }
    }
    
    [System.CodeDom.Compiler.GeneratedCodeAttribute("Microsoft.Tools.ServiceModel.Svcutil", "2.0.2")]
    [System.ServiceModel.ServiceContractAttribute(ConfigurationName="ServiceReference1.IService1")]
    public interface IService1
    {
        
        [System.ServiceModel.OperationContractAttribute(Action="http://tempuri.org/IService1/vpis", ReplyAction="http://tempuri.org/IService1/vpisResponse")]
        System.Threading.Tasks.Task<int> vpisAsync(string ime, string geslo);
        
        [System.ServiceModel.OperationContractAttribute(Action="http://tempuri.org/IService1/vrniVseIgralce", ReplyAction="http://tempuri.org/IService1/vrniVseIgralceResponse")]
        System.Threading.Tasks.Task<ServiceReference1.Igralec[]> vrniVseIgralce();
        
        [System.ServiceModel.OperationContractAttribute(Action="http://tempuri.org/IService1/vrniVseKlube", ReplyAction="http://tempuri.org/IService1/vrniVseKlubeResponse")]
        System.Threading.Tasks.Task<ServiceReference1.Klub[]> vrniVseKlubeAsync();
        
        [System.ServiceModel.OperationContractAttribute(Action="http://tempuri.org/IService1/vrniIgralca", ReplyAction="http://tempuri.org/IService1/vrniIgralcaResponse")]
        System.Threading.Tasks.Task<ServiceReference1.Igralec> vrniIgralcaAsync(int id);
        
        [System.ServiceModel.OperationContractAttribute(Action="http://tempuri.org/IService1/vrniKlub", ReplyAction="http://tempuri.org/IService1/vrniKlubResponse")]
        System.Threading.Tasks.Task<ServiceReference1.Klub> vrniKlubAsync(int id);
        
        [System.ServiceModel.OperationContractAttribute(Action="http://tempuri.org/IService1/klubZNajvecIgralci", ReplyAction="http://tempuri.org/IService1/klubZNajvecIgralciResponse")]
        System.Threading.Tasks.Task<ServiceReference1.Klub> klubZNajvecIgralciAsync();
        
        [System.ServiceModel.OperationContractAttribute(Action="http://tempuri.org/IService1/najstarejsiIgralec", ReplyAction="http://tempuri.org/IService1/najstarejsiIgralecResponse")]
        System.Threading.Tasks.Task<ServiceReference1.Igralec> najstarejsiIgralecAsync();
        
        [System.ServiceModel.OperationContractAttribute(Action="http://tempuri.org/IService1/povprecnaStarostIgralcevVKlubu", ReplyAction="http://tempuri.org/IService1/povprecnaStarostIgralcevVKlubuResponse")]
        System.Threading.Tasks.Task<double> povprecnaStarostIgralcevVKlubuAsync(int id);
        
        [System.ServiceModel.OperationContractAttribute(Action="http://tempuri.org/IService1/dodajIgralca", ReplyAction="http://tempuri.org/IService1/dodajIgralcaResponse")]
        System.Threading.Tasks.Task dodajIgralcaAsync(string ime, string priimek, System.DateTime datumRojstva);
        
        [System.ServiceModel.OperationContractAttribute(Action="http://tempuri.org/IService1/dodajKlub", ReplyAction="http://tempuri.org/IService1/dodajKlubResponse")]
        System.Threading.Tasks.Task dodajKlubAsync(string naziv, string telovadnica, int letoUstanovitve);
        
        [System.ServiceModel.OperationContractAttribute(Action="http://tempuri.org/IService1/dodajIgralcuKlub", ReplyAction="http://tempuri.org/IService1/dodajIgralcuKlubResponse")]
        System.Threading.Tasks.Task dodajIgralcuKlubAsync(int igralecId, int klubId, int letoOd, int letoDo, int stNastopov);
        
        [System.ServiceModel.OperationContractAttribute(Action="http://tempuri.org/IService1/izbrisiIgralca", ReplyAction="http://tempuri.org/IService1/izbrisiIgralcaResponse")]
        System.Threading.Tasks.Task izbrisiIgralcaAsync(int id);
        
        [System.ServiceModel.OperationContractAttribute(Action="http://tempuri.org/IService1/izbrisiKlub", ReplyAction="http://tempuri.org/IService1/izbrisiKlubResponse")]
        System.Threading.Tasks.Task izbrisiKlubAsync(int id);
        
        [System.ServiceModel.OperationContractAttribute(Action="http://tempuri.org/IService1/odstraniIgralcaIzKluba", ReplyAction="http://tempuri.org/IService1/odstraniIgralcaIzKlubaResponse")]
        System.Threading.Tasks.Task odstraniIgralcaIzKlubaAsync(int igralecId, int klubId);
        
        [System.ServiceModel.OperationContractAttribute(Action="http://tempuri.org/IService1/spremeniKlub", ReplyAction="http://tempuri.org/IService1/spremeniKlubResponse")]
        System.Threading.Tasks.Task spremeniKlubAsync(int id, string naziv, string telovadnica, int letoUstanovitve);
        
        [System.ServiceModel.OperationContractAttribute(Action="http://tempuri.org/IService1/spremeniIgralca", ReplyAction="http://tempuri.org/IService1/spremeniIgralcaResponse")]
        System.Threading.Tasks.Task spremeniIgralcaAsync(int id, string ime, string priimek, System.DateTime datumRojstva);
        
        [System.ServiceModel.OperationContractAttribute(Action="http://tempuri.org/IService1/vrniVseIgralceVKlubih", ReplyAction="http://tempuri.org/IService1/vrniVseIgralceVKlubihResponse")]
        System.Threading.Tasks.Task<ServiceReference1.IgralecVKlubu[]> vrniVseIgralceVKlubihAsync();
    }
    
    [System.CodeDom.Compiler.GeneratedCodeAttribute("Microsoft.Tools.ServiceModel.Svcutil", "2.0.2")]
    public interface IService1Channel : ServiceReference1.IService1, System.ServiceModel.IClientChannel
    {
    }
    
    [System.Diagnostics.DebuggerStepThroughAttribute()]
    [System.CodeDom.Compiler.GeneratedCodeAttribute("Microsoft.Tools.ServiceModel.Svcutil", "2.0.2")]
    public partial class Service1Client : System.ServiceModel.ClientBase<ServiceReference1.IService1>, ServiceReference1.IService1
    {
        
        /// <summary>
        /// Implement this partial method to configure the service endpoint.
        /// </summary>
        /// <param name="serviceEndpoint">The endpoint to configure</param>
        /// <param name="clientCredentials">The client credentials</param>
        static partial void ConfigureEndpoint(System.ServiceModel.Description.ServiceEndpoint serviceEndpoint, System.ServiceModel.Description.ClientCredentials clientCredentials);
        
        public Service1Client() : 
                base(Service1Client.GetDefaultBinding(), Service1Client.GetDefaultEndpointAddress())
        {
            this.Endpoint.Name = EndpointConfiguration.BasicHttpBinding_IService1.ToString();
            ConfigureEndpoint(this.Endpoint, this.ClientCredentials);
        }
        
        public Service1Client(EndpointConfiguration endpointConfiguration) : 
                base(Service1Client.GetBindingForEndpoint(endpointConfiguration), Service1Client.GetEndpointAddress(endpointConfiguration))
        {
            this.Endpoint.Name = endpointConfiguration.ToString();
            ConfigureEndpoint(this.Endpoint, this.ClientCredentials);
        }
        
        public Service1Client(EndpointConfiguration endpointConfiguration, string remoteAddress) : 
                base(Service1Client.GetBindingForEndpoint(endpointConfiguration), new System.ServiceModel.EndpointAddress(remoteAddress))
        {
            this.Endpoint.Name = endpointConfiguration.ToString();
            ConfigureEndpoint(this.Endpoint, this.ClientCredentials);
        }
        
        public Service1Client(EndpointConfiguration endpointConfiguration, System.ServiceModel.EndpointAddress remoteAddress) : 
                base(Service1Client.GetBindingForEndpoint(endpointConfiguration), remoteAddress)
        {
            this.Endpoint.Name = endpointConfiguration.ToString();
            ConfigureEndpoint(this.Endpoint, this.ClientCredentials);
        }
        
        public Service1Client(System.ServiceModel.Channels.Binding binding, System.ServiceModel.EndpointAddress remoteAddress) : 
                base(binding, remoteAddress)
        {
        }
        
        public System.Threading.Tasks.Task<int> vpisAsync(string ime, string geslo)
        {
            return base.Channel.vpisAsync(ime, geslo);
        }
        
        public System.Threading.Tasks.Task<ServiceReference1.Igralec[]> vrniVseIgralce()
        {
            return base.Channel.vrniVseIgralce();
        }

        public System.Threading.Tasks.Task<ServiceReference1.Igralec[]> vrniVseIgralceAsync()
        {
            return base.Channel.vrniVseIgralce();
        }

        public System.Threading.Tasks.Task<ServiceReference1.Klub[]> vrniVseKlubeAsync()
        {
            return base.Channel.vrniVseKlubeAsync();
        }
        
        public System.Threading.Tasks.Task<ServiceReference1.Igralec> vrniIgralcaAsync(int id)
        {
            return base.Channel.vrniIgralcaAsync(id);
        }
        
        public System.Threading.Tasks.Task<ServiceReference1.Klub> vrniKlubAsync(int id)
        {
            return base.Channel.vrniKlubAsync(id);
        }
        
        public System.Threading.Tasks.Task<ServiceReference1.Klub> klubZNajvecIgralciAsync()
        {
            return base.Channel.klubZNajvecIgralciAsync();
        }
        
        public System.Threading.Tasks.Task<ServiceReference1.Igralec> najstarejsiIgralecAsync()
        {
            return base.Channel.najstarejsiIgralecAsync();
        }
        
        public System.Threading.Tasks.Task<double> povprecnaStarostIgralcevVKlubuAsync(int id)
        {
            return base.Channel.povprecnaStarostIgralcevVKlubuAsync(id);
        }
        
        public System.Threading.Tasks.Task dodajIgralcaAsync(string ime, string priimek, System.DateTime datumRojstva)
        {
            return base.Channel.dodajIgralcaAsync(ime, priimek, datumRojstva);
        }
        
        public System.Threading.Tasks.Task dodajKlubAsync(string naziv, string telovadnica, int letoUstanovitve)
        {
            return base.Channel.dodajKlubAsync(naziv, telovadnica, letoUstanovitve);
        }
        
        public System.Threading.Tasks.Task dodajIgralcuKlubAsync(int igralecId, int klubId, int letoOd, int letoDo, int stNastopov)
        {
            return base.Channel.dodajIgralcuKlubAsync(igralecId, klubId, letoOd, letoDo, stNastopov);
        }
        
        public System.Threading.Tasks.Task izbrisiIgralcaAsync(int id)
        {
            return base.Channel.izbrisiIgralcaAsync(id);
        }
        
        public System.Threading.Tasks.Task izbrisiKlubAsync(int id)
        {
            return base.Channel.izbrisiKlubAsync(id);
        }
        
        public System.Threading.Tasks.Task odstraniIgralcaIzKlubaAsync(int igralecId, int klubId)
        {
            return base.Channel.odstraniIgralcaIzKlubaAsync(igralecId, klubId);
        }
        
        public System.Threading.Tasks.Task spremeniKlubAsync(int id, string naziv, string telovadnica, int letoUstanovitve)
        {
            return base.Channel.spremeniKlubAsync(id, naziv, telovadnica, letoUstanovitve);
        }
        
        public System.Threading.Tasks.Task spremeniIgralcaAsync(int id, string ime, string priimek, System.DateTime datumRojstva)
        {
            return base.Channel.spremeniIgralcaAsync(id, ime, priimek, datumRojstva);
        }
        
        public System.Threading.Tasks.Task<ServiceReference1.IgralecVKlubu[]> vrniVseIgralceVKlubihAsync()
        {
            return base.Channel.vrniVseIgralceVKlubihAsync();
        }
        
        public virtual System.Threading.Tasks.Task OpenAsync()
        {
            return System.Threading.Tasks.Task.Factory.FromAsync(((System.ServiceModel.ICommunicationObject)(this)).BeginOpen(null, null), new System.Action<System.IAsyncResult>(((System.ServiceModel.ICommunicationObject)(this)).EndOpen));
        }
        
        public virtual System.Threading.Tasks.Task CloseAsync()
        {
            return System.Threading.Tasks.Task.Factory.FromAsync(((System.ServiceModel.ICommunicationObject)(this)).BeginClose(null, null), new System.Action<System.IAsyncResult>(((System.ServiceModel.ICommunicationObject)(this)).EndClose));
        }
        
        private static System.ServiceModel.Channels.Binding GetBindingForEndpoint(EndpointConfiguration endpointConfiguration)
        {
            if ((endpointConfiguration == EndpointConfiguration.BasicHttpBinding_IService1))
            {
                System.ServiceModel.BasicHttpBinding result = new System.ServiceModel.BasicHttpBinding();
                result.MaxBufferSize = int.MaxValue;
                result.ReaderQuotas = System.Xml.XmlDictionaryReaderQuotas.Max;
                result.MaxReceivedMessageSize = int.MaxValue;
                result.AllowCookies = true;
                return result;
            }
            throw new System.InvalidOperationException(string.Format("Could not find endpoint with name \'{0}\'.", endpointConfiguration));
        }
        
        private static System.ServiceModel.EndpointAddress GetEndpointAddress(EndpointConfiguration endpointConfiguration)
        {
            if ((endpointConfiguration == EndpointConfiguration.BasicHttpBinding_IService1))
            {
                return new System.ServiceModel.EndpointAddress("http://localhost:51170/Service1.svc");
            }
            throw new System.InvalidOperationException(string.Format("Could not find endpoint with name \'{0}\'.", endpointConfiguration));
        }
        
        private static System.ServiceModel.Channels.Binding GetDefaultBinding()
        {
            return Service1Client.GetBindingForEndpoint(EndpointConfiguration.BasicHttpBinding_IService1);
        }
        
        private static System.ServiceModel.EndpointAddress GetDefaultEndpointAddress()
        {
            return Service1Client.GetEndpointAddress(EndpointConfiguration.BasicHttpBinding_IService1);
        }

        public enum EndpointConfiguration
        {
            
            BasicHttpBinding_IService1,
        }
    }
}
