﻿using Microsoft.Win32;
using System;
using System.Collections.Generic;
using System.IO;
using System.Text;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;

namespace WpfApp2
{
    /// <summary>
    /// Interaction logic for DataGridWindow.xaml
    /// </summary>
    public partial class DataGridWindow : Window
    {
        static ServiceReference1.Service1Client servis;
        public DataGridWindow()
        {
            InitializeComponent();
            servis = new ServiceReference1.Service1Client();
            loadData();
        }

        public async void loadData() {
            DataGridIgralci.ItemsSource = await servis.vrniVseIgralceAsync();
            DataGridKlubi.ItemsSource = await servis.vrniVseKlubeAsync();
            DataGridIgralciVKlubih.ItemsSource = await servis.vrniVseIgralceVKlubihAsync();
        }

        private void dodajIgralca_click(object sender, RoutedEventArgs e)
        {
            IgralciWindow dgw = new IgralciWindow(this);
            dgw.Show();
            this.Hide();
        }

        private void dodajKlub_click(object sender, RoutedEventArgs e)
        {
            KlubiWindow dgw = new KlubiWindow(this);
            dgw.Show();
            this.Hide();
        }

        private void dodajIgralciVKlubih_click(object sender, RoutedEventArgs e)
        {
            IgralciVKlubihWindow dgw = new IgralciVKlubihWindow(this);
            dgw.Show();
            this.Hide();
        }

        private void Izvoz_Click(object sender, RoutedEventArgs e)
        {
            DataGridIgralci.SelectAllCells();
            DataGridIgralci.ClipboardCopyMode = DataGridClipboardCopyMode.IncludeHeader;
            ApplicationCommands.Copy.Execute(null, DataGridIgralci);
            DataGridIgralci.UnselectAllCells();
            String result = (string)Clipboard.GetData(DataFormats.CommaSeparatedValue);
            File.AppendAllText("D:\\igralci.csv", result, UnicodeEncoding.UTF8);
        }

        private void Tabcontrol_Click(object sender, RoutedEventArgs e)
        {
            TabControlWindow dgw = new TabControlWindow(this);
            dgw.Show();
            this.Hide();
        }

        private void Izvozklubov_Click(object sender, RoutedEventArgs e)
        {
            DataGridKlubi.SelectAllCells();
            DataGridKlubi.ClipboardCopyMode = DataGridClipboardCopyMode.IncludeHeader;
            ApplicationCommands.Copy.Execute(null, DataGridKlubi);
            DataGridKlubi.UnselectAllCells();
            String result = (string)Clipboard.GetData(DataFormats.CommaSeparatedValue);
            File.AppendAllText("D:\\klubi.csv", result, UnicodeEncoding.UTF8);
        }

        private void Izvozigralcevvklubih_Click(object sender, RoutedEventArgs e)
        {
            DataGridIgralciVKlubih.SelectAllCells();
            DataGridIgralciVKlubih.ClipboardCopyMode = DataGridClipboardCopyMode.IncludeHeader;
            ApplicationCommands.Copy.Execute(null, DataGridIgralciVKlubih);
            DataGridIgralciVKlubih.UnselectAllCells();
            String result = (string)Clipboard.GetData(DataFormats.CommaSeparatedValue);
            File.AppendAllText("D:\\Igralci_V_Klubih.csv", result, UnicodeEncoding.UTF8);
        }
    }
}
