﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;

namespace WpfApp2
{
    /// <summary>
    /// Interaction logic for IgralciVKlubihWindow.xaml
    /// </summary>
    public partial class IgralciVKlubihWindow : Window
    {
        DataGridWindow dataGridWindow;
        static ServiceReference1.Service1Client servis;
        public IgralciVKlubihWindow(DataGridWindow dgw)
        {
            InitializeComponent();
            servis = new ServiceReference1.Service1Client();
            this.dataGridWindow = dgw;
            loadComboBox();

        }

        private async void loadComboBox() {
            var vsiIgralci = await servis.vrniVseIgralceAsync();
            foreach (var igralec in vsiIgralci)
            {
                IgralecComboBox.Items.Add((new KeyValuePair<int, string>(igralec.Id, igralec.Ime + " " + igralec.Priimek)));
            }

            var vsiKlubi = await servis.vrniVseKlubeAsync();
            foreach (var klub in vsiKlubi)
            {
                KlubComboBox.Items.Add((new KeyValuePair<int, string>(klub.Id, klub.Naziv)));
            }
        }

        public async void dodaj_click(object sender, RoutedEventArgs e)
        {
            var igralecKey = (KeyValuePair<int, string>)IgralecComboBox.SelectedValue;
            var klubKey = (KeyValuePair<int, string>)KlubComboBox.SelectedValue;
            var igralecId = igralecKey.Key;
            var klubId = klubKey.Key;
            var letoOd = Convert.ToInt32(LetoOdTextBox.Text);
            var letoDo = Convert.ToInt32(LetoDoTextBox.Text);
            var stNastpov = Convert.ToInt32(StNastopovTextBox.Text);

            servis.dodajIgralcuKlubAsync(igralecId,klubId,letoOd,letoDo,stNastpov);
            dataGridWindow.loadData();
            this.Close();
            dataGridWindow.Show();
        }
    }
}
