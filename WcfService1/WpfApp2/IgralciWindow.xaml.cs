﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;

namespace WpfApp2
{
    /// <summary>
    /// Interaction logic for IgralciWindow.xaml
    /// </summary>
    public partial class IgralciWindow : Window
    {
        DataGridWindow dataGridWindow;
        ServiceReference1.Service1Client servis;

        public IgralciWindow(DataGridWindow dataGridWindow)
        {
            servis = new ServiceReference1.Service1Client();
            this.dataGridWindow = dataGridWindow;
            InitializeComponent();
        }

        public async void dodaj_click(object sender, RoutedEventArgs e)
        {
            await servis.dodajIgralcaAsync(TextBoxIme.Text, TextBoxPriimek.Text, DatePickerDatum.DisplayDate);
            dataGridWindow.loadData();
            this.Close();
            dataGridWindow.Show();
        }
    }
}
