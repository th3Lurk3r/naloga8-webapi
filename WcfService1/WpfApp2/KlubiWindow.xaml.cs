﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;

namespace WpfApp2
{
    /// <summary>
    /// Interaction logic for KlubiWindow.xaml
    /// </summary>
    public partial class KlubiWindow : Window
    {
        DataGridWindow dataGridWindow;
        ServiceReference1.Service1Client servis;
        public KlubiWindow(DataGridWindow dataGridWindow)
        {
            servis = new ServiceReference1.Service1Client();
            this.dataGridWindow = dataGridWindow;
            InitializeComponent();
        }

        public async void dodaj_click(object sender, RoutedEventArgs e)
        {
            await servis.dodajKlubAsync(KlubTextBox.Text, TelovadnicaTextBox.Text, int.Parse(LetoUstanovitveTextBox.Text));
            dataGridWindow.loadData();
            this.Close();
            dataGridWindow.Show();
        }

        private void KlubTextBox_TextChanged(object sender, TextChangedEventArgs e)
        {
            if(string.IsNullOrEmpty(KlubTextBox.Text))
            {
                progress.Value = 0;
            }
            else
            {
                progress.Value = +33;
            }
            
        }

        private void TelovadnicaTextBox_TextChanged(object sender, TextChangedEventArgs e)
        {
            if (string.IsNullOrEmpty(TelovadnicaTextBox.Text))
            {
                progress.Value = 33;
            }
            else
            {
                progress.Value = +66;
            }
        }

        private void LetoUstanovitveTextBox_TextChanged(object sender, TextChangedEventArgs e)
        {
            if (string.IsNullOrEmpty(LetoUstanovitveTextBox.Text))
            {
                progress.Value = 66;
            }
            else
            {
                progress.Value = 100;
            }
        }
    }
}
