﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace WpfApp2
{
    /// <summary>
    /// Interaction logic for MainWindow.xaml
    /// </summary>
    public partial class MainWindow : Window
    {
        static ServiceReference1.Service1Client servis;

        public MainWindow()
        {
            InitializeComponent();
            servis = new ServiceReference1.Service1Client();
        }

        private async void prijava_click(object sender, RoutedEventArgs e)
        {

            string ime = TextBoxUporabniskoIme.Text;
            string geslo = PasswordBoxGeslo.Password;

            //pridobimo podatke iz strežnike o prijavi
            int status = await servis.vpisAsync(ime, geslo);

            if (status == 0 || PogojBox.IsChecked == false)
            {
                MessageBox.Show("Neuspešna prijava!", "Prijava");
            }
            else
            {
                DataGridWindow dgw = new DataGridWindow();
                dgw.Show();
                this.Hide();
            }
        }
    }
}
