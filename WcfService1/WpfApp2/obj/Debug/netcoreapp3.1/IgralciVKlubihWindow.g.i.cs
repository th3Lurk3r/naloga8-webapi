﻿#pragma checksum "..\..\..\IgralciVKlubihWindow.xaml" "{ff1816ec-aa5e-4d10-87f7-6f4963833460}" "3790488894297A539D3414DA3FA64B676EC3F5AD"
//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated by a tool.
//     Runtime Version:4.0.30319.42000
//
//     Changes to this file may cause incorrect behavior and will be lost if
//     the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

using System;
using System.Diagnostics;
using System.Windows;
using System.Windows.Automation;
using System.Windows.Controls;
using System.Windows.Controls.Primitives;
using System.Windows.Controls.Ribbon;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Ink;
using System.Windows.Input;
using System.Windows.Markup;
using System.Windows.Media;
using System.Windows.Media.Animation;
using System.Windows.Media.Effects;
using System.Windows.Media.Imaging;
using System.Windows.Media.Media3D;
using System.Windows.Media.TextFormatting;
using System.Windows.Navigation;
using System.Windows.Shapes;
using System.Windows.Shell;
using WpfApp2;


namespace WpfApp2 {
    
    
    /// <summary>
    /// IgralciVKlubihWindow
    /// </summary>
    public partial class IgralciVKlubihWindow : System.Windows.Window, System.Windows.Markup.IComponentConnector {
        
        
        #line 20 "..\..\..\IgralciVKlubihWindow.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.ComboBox IgralecComboBox;
        
        #line default
        #line hidden
        
        
        #line 22 "..\..\..\IgralciVKlubihWindow.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.ComboBox KlubComboBox;
        
        #line default
        #line hidden
        
        
        #line 24 "..\..\..\IgralciVKlubihWindow.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.TextBox LetoOdTextBox;
        
        #line default
        #line hidden
        
        
        #line 25 "..\..\..\IgralciVKlubihWindow.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.Slider sliderletoOd;
        
        #line default
        #line hidden
        
        
        #line 27 "..\..\..\IgralciVKlubihWindow.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.TextBox LetoDoTextBox;
        
        #line default
        #line hidden
        
        
        #line 28 "..\..\..\IgralciVKlubihWindow.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.Slider sliderletodo;
        
        #line default
        #line hidden
        
        
        #line 30 "..\..\..\IgralciVKlubihWindow.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.TextBox StNastopovTextBox;
        
        #line default
        #line hidden
        
        
        #line 33 "..\..\..\IgralciVKlubihWindow.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.Button Tipka;
        
        #line default
        #line hidden
        
        private bool _contentLoaded;
        
        /// <summary>
        /// InitializeComponent
        /// </summary>
        [System.Diagnostics.DebuggerNonUserCodeAttribute()]
        [System.CodeDom.Compiler.GeneratedCodeAttribute("PresentationBuildTasks", "5.0.11.0")]
        public void InitializeComponent() {
            if (_contentLoaded) {
                return;
            }
            _contentLoaded = true;
            System.Uri resourceLocater = new System.Uri("/WpfApp2;component/igralcivklubihwindow.xaml", System.UriKind.Relative);
            
            #line 1 "..\..\..\IgralciVKlubihWindow.xaml"
            System.Windows.Application.LoadComponent(this, resourceLocater);
            
            #line default
            #line hidden
        }
        
        [System.Diagnostics.DebuggerNonUserCodeAttribute()]
        [System.CodeDom.Compiler.GeneratedCodeAttribute("PresentationBuildTasks", "5.0.11.0")]
        [System.ComponentModel.EditorBrowsableAttribute(System.ComponentModel.EditorBrowsableState.Never)]
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Design", "CA1033:InterfaceMethodsShouldBeCallableByChildTypes")]
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Maintainability", "CA1502:AvoidExcessiveComplexity")]
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1800:DoNotCastUnnecessarily")]
        void System.Windows.Markup.IComponentConnector.Connect(int connectionId, object target) {
            switch (connectionId)
            {
            case 1:
            this.IgralecComboBox = ((System.Windows.Controls.ComboBox)(target));
            return;
            case 2:
            this.KlubComboBox = ((System.Windows.Controls.ComboBox)(target));
            return;
            case 3:
            this.LetoOdTextBox = ((System.Windows.Controls.TextBox)(target));
            return;
            case 4:
            this.sliderletoOd = ((System.Windows.Controls.Slider)(target));
            return;
            case 5:
            this.LetoDoTextBox = ((System.Windows.Controls.TextBox)(target));
            return;
            case 6:
            this.sliderletodo = ((System.Windows.Controls.Slider)(target));
            return;
            case 7:
            this.StNastopovTextBox = ((System.Windows.Controls.TextBox)(target));
            return;
            case 8:
            this.Tipka = ((System.Windows.Controls.Button)(target));
            
            #line 33 "..\..\..\IgralciVKlubihWindow.xaml"
            this.Tipka.Click += new System.Windows.RoutedEventHandler(this.dodaj_click);
            
            #line default
            #line hidden
            return;
            }
            this._contentLoaded = true;
        }
    }
}

